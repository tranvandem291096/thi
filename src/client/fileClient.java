/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.net.InetAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author 24h
 * interface này tạo ra cấu trúc nhận địa chỉ, thay đổi trạng thái đồng 
 * bộ hóa
 */
public interface fileClient extends Remote {

    /**
     *
     * @trả về địa chỉ
     */
    public InetAddress getAddress() throws RemoteException;
    
    /**
    * thay đổi trạng thái của đồng bộ hóa
    */
    public void setSyncState(int state) throws RemoteException;
    /**
     * lấy ra trạng thái của đồng bộ hóa
    */
    public String getSyncState() throws RemoteException;
    
    
}
